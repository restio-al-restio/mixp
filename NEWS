NEWS for Mixp
See the end for copying conditions.


- 0.9 | 2021-12-16

  - Guile "auto-compilation" suppressed

    Env var ‘GUILE_AUTO_COMPILE’ has value "0" during both configure
    script operation and "make check".  The Guile-BAUX programs also
    set this, so normally "make" should also be covered.

    The result: No on-the-fly compilation of .scm to .go files.

  - configure script option ‘--with-expat’ has default

    You can use this option to specify a non-standard installation.
    Previously, it was required to be given since there was no
    default.  Now, if not given, it defaults to the system Expat.

  - configure script errors out if Expat not found

    If there is no usable Expat specified or found, the configure
    script now fails w/ message "Could not find the Expat library".

  - testing upgraded to support Guile 2.2

    For "make check", Guile 2.2 had some issues grokking the
    iso-8859-1 encoding in test/x001, but that's now resolved.

  - bootstrap/maintenance tools

    upgraded:

     Guile-BAUX 20211208.0839.a5245e7
     GNU gnulib 2021-12-10 21:54:54
     GNU texinfo 6.8
     GNU Automake 1.16.5
     GNU Autoconf 2.71
     GNU Guile 2.2.7

    as before:

     (none)


- 0.8 | 2020-10-22

  - bug fix: use ‘GCMALLOC’, not ‘malloc’

    Regression introduced in Mixp 0.6 (released 2012-01-25).

  - backward-incompatible change in ‘(mixp utils) xml->tree’

    The proc ‘(mixp utils) xml->tree’ no longer sets the
    ‘unparsed-entity-decl’ handler.  This is in prepraration for the
    eventual dropping of ‘unparsed-entity-decl’ handler everywhere.

  - distribution now .tar.lz

    If you have GNU tar, you can use "tar xf" and it will DTRT.
    If not, you can use "lzip -dc TARBALL | tar xf -" to unpack.

  - new ‘(mixp expat)’ items

    - handlers ‘doctype-decl-start’, ‘doctype-decl-end’

      These are called for doctype declarations: "<!DOCTYPE ...>".

    - handler ‘skipped-entity’

      This is called in two situations:

      1. An entity reference is encountered for which no declaration
      has been read *and* this is not an error.

      2. An internal entity reference is read, but not expanded,
      because ‘default-handler’ has been used.

    - handler ‘entity-decl’

      This is called for entity declarations: "<!ENTITY ...>".

    - procedure ‘get-feature-list’

      This returns an alist of features, some of which a corresponding
      positive integer value.  As a special case, the first feature is
      "EXPAT_VERSION".  See (info "(mixp) Expat interface").

    - procedure ‘get-attribute-info’

      This returns attribute offset information in the form of a 2-D
      typed-array (u64), if the underlying ‘XML_GetAttributeInfo’ is
      available, else ‘#f’.  See (info "(mixp) Expat misc").

    - procedure ‘expat-version’

      This returns the Expat version in various ways.

    - procedure ‘set-encoding’

      This supports parser creation and setting the input encoding in
      two steps, as opposed to in one step, which may be useful when
      the encoding is not known at ‘parser-create’ time.

    - procedures ‘stop-parser’, ‘resume-parser’

      It's possible to now to "suspend" a parse, w/ these procs.  See
      test/x001 for example.

    - procedure ‘get-parsing-status’

      This is useful to confirm that a parse is suspended, and to find
      out whether or not the last chunk is being processed.

    - procedure ‘set-hash-salt’

      Apparently, people can DoS Expat by maliciously guessing (and
      poking) its internal hashing patterns.  This procedure helps to
      mitigate against such abuse.

    - procedure ‘hget-one’

      This returns the procedure set as a parser object's handler.
      See (info "(mixp) Expat handlers").

  - support for error codes up through Expat 2.2.1

    I.e., if the underlying libexpat is 2.2.1 or later, you might
    receive the error message ‘XML_ERROR_INVALID_ARGUMENT’, among
    any others.

  - planned retirement

    Three ‘(mixp expat)’ items WILL BE REMOVED by the time of the
    1.0 release of Mixp (could be a year or more, realistically
    speaking).  OTOH, it could be next release.  Luckily, there is a
    migration path for all cases.  Please do so as soon as possible.

    - procedure ‘geth’

      This procedure returns an alist w/ all the named handlers as
      keys, which may or may not be what you want.  You can use new
      proc ‘geth-one’ (see above) to find the same information, and
      package it in a more flexible manner (alist, plist, normal list,
      subset, etc).

    - handler ‘unparsed-entity-decl’

      This is obsoleted by the new handler ‘entity-decl’ (see above)
      both in Mixp and in the underlying Expat.  Be sure to avoid
      specifying both at the same time, as ‘unparsed-entity-decl’ will
      take precedence over ‘entity-decl’.

    - alist support for ‘hset!’

      Taking an alist is convenient for the implementation, but
      inconvenient for the user, typically requiring ‘quasiquote’ and
      abundant ‘unquote’ gymnastics.  As of this release, ‘hset!’ also
      takes a plist, so support for taking an alist can be deprecated.
      If your code currently does:

       (hset! my-parser `((element-start . ,handle-elem-start)
                          (element-end   . ,handle-elem-end)))

      you should change it to read:

       (hset! my-parser
              'element-start handle-elem-start
              'element-end   handle-elem-end)

      As you can see, less hair this way.

  - nice coding cookie for Guile 2

    The "coding cookie" in test/x001 is now "iso-8859-1", which
    should be friendlier to Guile 2.

  - bug report email address changed

    It is now <guile-user@gnu.org>, which should have a better
    chance and higher quality of response for technical questions
    than the previous one.

  - doc improvements

    - ‘set-base’

      The type of the second arg BASE is now documented.

    - ‘set-param-entity-parsing’

      Proc ‘set-param-entity-parsing’ takes argument CODE, which is
      now documented.

    - misc upgrades

      The top node now includes a detailed node listing and copyright
      information for PDF output.  The bug report email address is now
      documented.  Copying info is now presented in a standard way.

  - nicer output for "make check TRACE=1"

    The ‘character-data’ blocks w/ solely linefeed ("\n") are now
    displayed that way instead of directly (w/ a linefeed char).
    This is easier on the eyes.

  - miscellaneous changes for Guile 2

    This is the first release for which the development environment
    is completely transitioned to Guile 2 (2.0.13, to be precise).
    Lots of slogging w/ ‘GUILE_WARN_DEPRECATED=detailed’, IOW.
    [Insert curmudgeonly rant, here.]  If you find other warnings,
    please report them as bugs.

    Note, however, that we still roll w/ ‘GUILE_AUTO_COMPILE=0’, so
    any problems you see w/ that aspect of Guile 2, please save for
    the next release, when we plan to brave those waters sul serio.
    (OTOH, it can't hurt to raise issues, as long as someone can
    clear the earwax and be nudged to DTRT -- fingers crossed!)

  - bootstrap/maintenance tools

    upgraded:

     Guile-BAUX 20200129.1051.1679aca
     GNU gnulib 2020-10-19 23:37:09
     GNU texinfo 6.7
     GNU Automake 1.16.2
     GNU Autoconf 2.69c
     GNU Guile 2.0.13

    as before:

     (none)


- 0.7 | 2013-03-10

  - distribution now .tar.xz

    If you have GNU tar, you can use "tar xf" and it will DTRT.
    If not, you can use "xz -dc TARBALL | tar xf -" to unpack.

  - bootstrap tools upgraded
    - GNU Autoconf 2.69
    - GNU Automake 1.13.1
    - Guile-BAUX 20121120.1242.e233fad
    - SNUGGLE 0.2
    - GNU Texinfo 5.0


- 0.6 | 2012-01-25
  - new EXPERIMENTAL module ‘(mixp simit)’
  - improved portability (tested w/ Guile 1.8.7)


- 0.5 | 2011-08-05
  - maintainer now Thien-Thi Nguyen (please see THANKS)
  - license now GPLv3+
  - dropped $(bindir)/guile-expat
  - modules reset (BACKWARD INCOMPATIBLE)
    - (mixp expat) replaces (xml expat)
      - bugfix: handle NULL ‘prefix’ for ‘namespace-decl-start’
      - dropped
        - "expat:" prefix for exports
        - proc ‘set-encoding’
      - symbolic status, error codes
      - new error codes from expat 2.0
      - condensed API for handlers: ‘hset!’, ‘hget’
      - ‘not-standalone’ handler rv now interpreted as boolean
    - (mixp utils) replaces (xml mixp)
      - dropped
        - "mixp:" prefix for exports
        - proc ‘xml->list’
        - tree <-> list procs
      - ‘xml->tree’ handles ‘processing-instruction’, ‘comment’
  - documentation improvements
    - license now GFDL 1.3
    - indices combined
    - toc at beginning :-D
    - (slightly) more examples
  - "make check" improvements
    - does in-tree ".fake" install
    - honors VERBOSE=1
  - tested with (Unofficial) Guile 1.4.1.123
  - tested with GNU Guile 1.8.7
  - infrastructure (configure.ac, Makefile.am files) upgraded


- 0.4 | 2003-01-15
  - maintainer now Dmitry Morozhnikov
  - fixed to work with Guile 1.6


- etc
Copyright (C) 2011-2013, 2020, 2021 Thien-Thi Nguyen

Copying and distribution of this file, with or without modification,
are permitted provided the copyright notice and this notice are preserved.

	Local Variables:
	mode: outline
	outline-regexp: "\\([ ][ ]\\)*- "
	End:
